# Moved to GitHub

This module migrated to https://github.com/atlassian/docker-infrastructure due to [JPERF-375].

[JPERF-375]: https://ecosystem.atlassian.net/browse/JPERF-375
